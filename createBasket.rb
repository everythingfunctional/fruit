#!/usr/bin/env ruby

require_relative 'fruit_processor'

unless ARGV.length >= 2
    puts "Usage: "
    puts "createBasket.rb basket_file test_suite_1 [test_suite_2 ...]"
    exit
end

createBasket(ARGV[0], ARGV[1..-1])
