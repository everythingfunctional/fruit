#!/usr/bin/env ruby

require_relative 'fruit_processor'

unless ARGV.length == 3
    puts "Usage: "
    puts "createDriver.rb driver_file basket_file results_file"
    exit
end

createDriver(ARGV[0], ARGV[1], ARGV[2])
